const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

let datos = [
	{
		matricula: "2020030301",
		nombre: "Margarita Lizarraga",
		sexo: "M",
		materias: ["Ingles", "Bse de datos", "Tecnologias I"],
	},
	{
		matricula: "2020030314",
		nombre: "Alejandro Trejo",
		sexo: "H",
		materias: ["Ingles", "Bse de datos", "Tecnologias I"],
	},
	{
		matricula: "2020030101",
		nombre: "Ariana Trejo",
		sexo: "M",
		materias: ["Ingles", "Base de datos", "Tecnologias I"],
	},
];


// Cotización

router.get("/cotizacion", (req, res) => {
	const params = {
		numB: req.body.numB,
		destino: req.body.destino,
		nombreCliente: req.body.nombreCliente,
		años: req.body.años,
		tipo: req.body.tipo,
		precio: req.body.precio,
	};
	res.render("cotizacion.html", params);
});

router.post("/cotizacion", (req, res) => {
	const params = {
		numB: req.body.numB,
		destino: req.body.destino,
		nombreCliente: req.body.nombreCliente,
		años: req.body.años,
		tipo: req.body.tipo,
		precio: req.body.precio,
	};
	res.render("cotizacion.html", params);
});



//LA PAGINA ERROR VA AL FINAL DE GET/ POST

router.get("/", (req, res) => {
	res.render("index.html", { titulo: "Actividad PreExamen", nombre: "Alejandro Lizarraga Motta", grupo: "8-3", listado: datos });
});

module.exports = router;
